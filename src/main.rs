extern crate zmq_sys;
use clap::{crate_authors, crate_version, App, Arg};
use pcap;
use zmq;

static SNDMORE: i32 = zmq_sys::ZMQ_SNDMORE as i32;

fn main() {
    let app = App::new("pwfnode")
        .about("pwfnode")
        .version(crate_version!())
        .author(crate_authors!())
        .arg(
            Arg::with_name("node")
                .long("node")
                .short("n")
                .takes_value(true)
                .empty_values(false)
                .help("node name (for example: sensor00"),
        )
        .arg(
            Arg::with_name("interface")
                .long("interface")
                .short("i")
                .takes_value(true)
                .empty_values(false)
                .help("inferface to capture (for example: mon0)"),
        )
        .arg(
            Arg::with_name("zmq")
                .long("zmq")
                .short("z")
                .takes_value(true)
                .empty_values(false)
                .help("zmq endpoint (for example: tcp://127.0.0.1:5555)"),
        )
        .arg(
            Arg::with_name("filter")
                .long("filter")
                .short("f")
                .takes_value(true)
                .empty_values(false)
                .help("packet filter"),
        )
        .arg(
            Arg::with_name("timeout")
                .long("timeout")
                .short("t")
                .takes_value(true)
                .empty_values(false)
                .help("packet capture timeout in ms (default: 1)"),
        );

    let app_args = app.get_matches();

    let nodename = String::from(app_args.value_of("node").unwrap_or("default"));
    let interface = String::from(app_args.value_of("interface").unwrap_or("mon0"));
    let pcap_filter = String::from(app_args.value_of("filter").unwrap_or(""));
    let pcap_timeout: i32 = app_args.value_of("timeout").unwrap_or("1").parse().unwrap();
    let zmq_endpoint = String::from(app_args.value_of("zmq").expect("zmq endpoint required"));

    let version: Vec<u8> = vec![1];
    let datalink = String::from("radiotap");

    let context = zmq::Context::new();
    let ingest = context.socket(zmq::PUSH).unwrap();
    ingest
        .connect(zmq_endpoint.as_str())
        .expect("failed to connect");

    let mut cap = pcap::Capture::from_device(interface.as_str())
        .unwrap()
        .promisc(true)
//        .snaplen(2048)
//        .buffer_size(4096)
        .timeout(pcap_timeout)
        .open()
        .unwrap();
    if pcap_filter != "" {
        cap.filter(pcap_filter.as_str()).expect("invalid filter");
    }

    while let Ok(packet) = cap.next() {
//        println!("{:?}", packet);
        ingest.send(&version, SNDMORE).unwrap();
        ingest.send(&nodename, SNDMORE).unwrap();
        ingest.send(&interface, SNDMORE).unwrap();
        ingest.send(&datalink, SNDMORE).unwrap();
        ingest.send(&packet.data, 0).unwrap();
    }
}
